# -*- coding: utf-8 -*-

from openerp import models, fields

class mrp_bom_component(models.Model):
    _name = 'mrp.bom.component'

    name = fields.Char(string="Name")


class mrp_bom_line(models.Model):
    _inherit = 'mrp.bom.line'

    bom_component_id = fields.Many2one('mrp.bom.component', string="Bom component")
